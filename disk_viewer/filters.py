from disk_viewer.models import (
    Server,
    Disk
)

from django_filters import rest_framework as filters


class ServerFilter(filters.FilterSet):
    hostname = filters.CharFilter(field_name='hostname',
                                  lookup_expr='icontains')

    address = filters.CharFilter(field_name='address',
                                 lookup_expr='icontains')
    location = filters.CharFilter(field_name='location',
                                  lookup_expr='icontains')

    class Meta:
        model = Server
        fields = ['hostname',
                  'address',
                  'location']


class DiskFilter(filters.FilterSet):
    serial_number = filters.CharFilter(field_name='serial_number',
                                       lookup_expr='icontains')
    status = filters.CharFilter(field_name='status',
                                lookup_expr='icontains')
    health = filters.CharFilter(field_name='health',
                                lookup_expr='icontains')
    host = filters.CharFilter(field_name='server__hostname',
                              lookup_expr='icontains')

    class Meta:
        model = Disk
        fields = ['serial_number', 'status', 'health', 'host']
