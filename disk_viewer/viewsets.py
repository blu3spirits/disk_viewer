from disk_viewer.models import (
    Disk,
    Server
)
from disk_viewer.filters import (
    DiskFilter,
    ServerFilter
)
from disk_viewer.serializers import (
    DiskSerializer,
    ServerSerializer
)

from rest_framework import viewsets
from django_filters import rest_framework as filters


class DiskViewSet(viewsets.ModelViewSet):
    serializer_class = DiskSerializer
    queryset = Disk.objects.all().select_related('server')
    filterset_class = DiskFilter
    filter_backends = (filters.DjangoFilterBackend,)


class ServerViewSet(viewsets.ModelViewSet):
    serializer_class = ServerSerializer
    queryset = Server.objects.all()
    filterset_class = ServerFilter
    filter_backends = (filters.DjangoFilterBackend,)
