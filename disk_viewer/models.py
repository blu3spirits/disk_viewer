from django.db import models

DISK_USAGE_CHOICES = [
    ('FRE', 'Free'),
    ('USD', 'Used'),
    ('UNU', 'Unused')
]

DISK_HEALTH_CHOICES = [
    ('OK', 'Okay'),
    ('WARN', 'Warning'),
    ('CRIT', 'Critical')
]


class Disk(models.Model):
    serial_number = models.CharField(primary_key=True,
                                     max_length=100,
                                     default=0,
                                     help_text='The serial number of the hdd')
    size = models.IntegerField(default=0,
                               help_text='The size in GB of the hdd')
    speed = models.IntegerField(default=0,
                                help_text='Speed of the disk in rpm, 0 if SSD')
    status = models.CharField(max_length=3,
                              choices=DISK_USAGE_CHOICES,
                              default='FRE')
    health = models.CharField(max_length=4,
                              choices=DISK_HEALTH_CHOICES,
                              default='OK')
    port = models.IntegerField(default=0,
                               blank=False,
                               null=False)
    server = models.ForeignKey('Server',
                               on_delete=models.CASCADE,
                               related_name='disk_server',
                               help_text='The server the disk is in')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.serial_number

    class Meta:
        ordering = ['port']
        verbose_name = 'disk'
        verbose_name_plural = 'disks'
        db_table = 'disks'


class Server(models.Model):
    hostname = models.CharField(max_length=50,
                                unique=True,
                                help_text='The hostname of the server')
    address = models.GenericIPAddressField(null=False)
    location = models.TextField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.hostname

    class Meta:
        ordering = ['hostname']
        verbose_name = 'server'
        verbose_name_plural = 'servers'
        db_table = 'servers'
