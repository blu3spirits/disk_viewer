from django.test import TestCase
from disk_viewer.models import (
    Disk
)

def DiskTestCase(TestCase):
    def setUp(self):
        Disk.objects.create(serial_number='000001',
                            size=8000,
                            disk_port=1)

    def test_disk_create(TestCase):
        Disk.objects.create(serial_number='000002',
                            size=8000,
                            disk_port=2)
