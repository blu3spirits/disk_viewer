from django.apps import AppConfig


class DiskViewerConfig(AppConfig):
    name = 'disk_viewer'
