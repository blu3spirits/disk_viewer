from disk_viewer.settings.base import *

# New prod key SET THIS BEFORE YOU RUN
SECRET_KEY = ''

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
    'rest_framework.renderers.JSONRenderer',
)
