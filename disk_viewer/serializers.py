from rest_framework import serializers

from disk_viewer.models import (
    Disk,
    Server
)


class ServerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Server
        fields = '__all__'


class DiskSerializer(serializers.ModelSerializer):
    host = serializers.CharField(source='server.hostname')

    class Meta:
        model = Disk
        fields = '__all__'
